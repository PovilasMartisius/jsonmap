
import XCTest
@testable import JSONMap


private struct CombinedStructureChild: JSONReadableDictionary {
    
    let value: String
    
    static func create(with json: [String : Any]) throws -> CombinedStructureChild {
        return CombinedStructureChild(value: try json.value(forKey: "testKey"))
    }
    
}

private struct CombinedStructureParent: JSONReadableDictionary {
    
    let value: String
    let child: CombinedStructureChild
    
    static func create(with json: [String : Any]) throws -> CombinedStructureParent {
        return CombinedStructureParent(value: try json.value(forKey: "testKey"),
                                       child: try CombinedStructureChild.create(with: try json.value(forKey: "testChild")))
    }
    
}

private struct DictionaryReadingStruct: JSONReadableDictionary {
    
    let value: String
    
    static func create(with json: [String : Any]) throws -> DictionaryReadingStruct {
        return DictionaryReadingStruct(value: try json.value(forKey: "testKey"))
    }
    
}

private struct DictionaryReadingStructOptional: JSONReadableDictionary {
    
    let value: String?
    
    static func create(with json: [String : Any]) throws -> DictionaryReadingStructOptional {
        return DictionaryReadingStructOptional(value: try json.optionalValue(forKey: "testKey"))
    }
    
}

class JSONMapTests: XCTestCase {
    
    func testPossibleKeysExtractionSuccess() {
        do {
            let expectedResult = "testValue"
            let json1 = ["testKey1" : expectedResult,
                         "testKey2" : expectedResult]
            let json2 = ["testKey2" : expectedResult]
            
            let result1: String = try json1.value(withPossibleKeys: ["testKey1"])
            XCTAssertEqual(result1, expectedResult)
            
            let result2: String = try json1.value(withPossibleKeys: ["testKey2"])
            XCTAssertEqual(result2, expectedResult)
            
            let result3: String = try json2.value(withPossibleKeys: ["testKey1", "testKey2"])
            XCTAssertEqual(result3, expectedResult)
        } catch {
            XCTAssert(false)
        }
    }
    
    func testPossibleKeysExtractionFailure() {
        do {
            let json = ["testKey1" : "testValue"]
            _ = try json.value(withPossibleKeys: ["testKey2"]) as String
            XCTAssert(false)
        } catch {
            
        }
    }
    
    func testMapSuccessRequired() {
        do {
            let json = ["testKey" : "testValue"]
            let result = try DictionaryReadingStruct.create(with: json)
            XCTAssertEqual(result.value, "testValue")
        } catch {
            XCTAssert(false)
        }
    }
    
    func testMapSuccessOptionalWithValue() {
        do {
            let json = ["testKey" : "testValue"]
            let result = try DictionaryReadingStructOptional.create(with: json)
            XCTAssertEqual(result.value, "testValue")
        } catch {
            XCTAssert(false)
        }
    }
    
    func testMapSuccessOptionalWithoutValue() {
        do {
            let result = try DictionaryReadingStructOptional.create(with: [ : ])
            XCTAssertNil(result.value)
        } catch {
            XCTAssert(false)
        }
    }
    
    func testMapFailureBadValueType() {
        do {
            let json = ["testKey" : 1]
            _ = try DictionaryReadingStruct.create(with: json)
            XCTAssert(false)
        } catch {
            switch error {
                
            case let JSONMapError.badValueType(key, value):
                XCTAssertEqual(key as? String, "testKey")
                XCTAssertEqual(value as? Int, 1)
                
            default:
                XCTAssert(false)
            }
        }
    }
    
    func testMapFailureBadValueTypeOptional() {
        do {
            let json = ["testKey" : 1]
            _ = try DictionaryReadingStructOptional.create(with: json)
            XCTAssert(false)
        } catch {
            switch error {
                
            case let JSONMapError.badValueType(key, value):
                XCTAssertEqual(key as? String, "testKey")
                XCTAssertEqual(value as? Int, 1)
                
            default:
                XCTAssert(false)
            }
        }
    }
    
    func testMapFailureNoValue() {
        do {
            _ = try DictionaryReadingStruct.create(with: [ : ])
            XCTAssert(false)
        } catch {
            switch error {
                
            case let JSONMapError.noValue(key):
                XCTAssertEqual(key as? String, "testKey")
                
            default:
                XCTAssert(false)
            }
        }
    }
    
    func testArrayReadingSuccessRequired() {
        let values = [["testKey" : "value1"], ["testKey" : "value2"]]
        do {
            let result = try DictionaryReadingStruct.createArray(with: values)
            XCTAssertEqual(result.count, 2)
            XCTAssertEqual(result[0].value, "value1")
            XCTAssertEqual(result[1].value, "value2")
        } catch {
            XCTAssert(false)
        }
    }
    
    func testArrayReadingSuccessOptional() {
        let values = [["testKey" : "value1"], ["testKey" : 2], [ : ], ["testKey" : "finalValue"]]
        
        let result = DictionaryReadingStruct.createArrayWithContinueOnError(with: values)
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result[0].value, "value1")
        XCTAssertEqual(result[1].value, "finalValue")
    }
    
    func testArrayReadingFailure() {
        let values = [["testKey" : "value1"], ["testKey" : 2]]
        do {
            _ = try DictionaryReadingStruct.createArray(with: values)
            XCTAssert(false)
        } catch {
            switch error {
                
            case let JSONMapError.badValueType(key, value):
                XCTAssertEqual(key as? String, "testKey")
                XCTAssertEqual(value as? Int, 2)
                
            default:
                XCTAssert(false)
            }
        }
    }
    
    func testArrayRequiredStringsReadingSuccess() {
        let values = ["value1", "value2", "value3"]
        do {
            let results = try String.createArray(with: values)
            XCTAssertEqual(results, values)
        } catch {
            XCTAssert(false)
        }
    }
    
    func testArrayOptionalStringsReadingSuccessWithOtherValue() {
        let values: [Any] = ["value1", 2, "value3"]
        
        let results = String.createArrayWithContinueOnError(with: values)
        XCTAssertEqual(["value1", "value3"], results)
    }
    
    func testArrayRequiredStringsReadingFailure() {
        let values: [Any] = ["value1", 2, "value3"]
        do {
            _ = try String.createArray(with: values)
            XCTAssert(false)
        } catch {
            switch error {
                
            case let JSONMapError.badValueType(key, value):
                XCTAssertNil(key)
                XCTAssertEqual(value as? Int, 2)
                
            default:
                XCTAssert(false)
            }
        }
    }
    
    func testTwoLevelsMapSuccess() {
        let childValue = ["testKey" : "childValue"]
        let parentValue: Any = ["testKey" : "parentValue", "testChild" : childValue]
        
        do {
            let result = try CombinedStructureParent.create(with: parentValue)
            XCTAssertEqual(result.value, "parentValue")
            XCTAssertEqual(result.child.value, "childValue")
        } catch {
            XCTAssert(false)
        }
    }
    
    func testTwoLevelsMapFailure() {
        let childValue = ["testKey" : 2]
        let parentValue: Any = ["testKey" : "parentValue", "testChild" : childValue]
        
        do {
            _ = try CombinedStructureParent.create(with: parentValue)
            XCTAssert(false)
        } catch {
            switch error {
                
            case let JSONMapError.badValueType(key, value):
                XCTAssertEqual(key as? String, "testKey")
                XCTAssertEqual(value as? Int, 2)
                
            default:
                XCTAssert(false)
            }
        }
    }
    
}
