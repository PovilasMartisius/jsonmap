
import Foundation

public protocol JSONReadableDictionary {
    
    static func create(with json: [String : Any]) throws -> Self
    
}

public extension JSONReadableDictionary {
    
    public static func create(with json: Any) throws -> Self {
        guard let jsonDictionary = json as? [String : Any] else {
            throw JSONMapError.unsuportedFormat
        }
        
        return try self.create(with: jsonDictionary)
    }
 
    public static func createArray(with json: [Any]) throws -> [Self] {
        return try json.map({ try self.create(with: $0) })
    }
    
    public static func createArrayWithContinueOnError(with json: [Any]) -> [Self] {
        let result: [Self?] = json.map({ object in
            do {
                return try self.create(with: object)
            } catch {
                return nil
            }
        })
        
        return result.filter({ $0 != nil }) as! [Self]
    }
    
}
