
import Foundation

public extension Dictionary where Key: ExpressibleByStringLiteral {
    
    public func value<T>(forKey key: Key) throws -> T {
        guard let value = self[key] else {
            throw JSONMapError.noValue(key: key)
        }
        
        if let convertedValue = value as? T {
            return convertedValue
        }
        
        throw JSONMapError.badValueType(key: key, value: value)
    }
    
    public func optionalValue<T>(forKey key: Key) throws -> T? {
        guard let value = self[key] else {
            return nil
        }
        
        guard (value as? NSNull) == nil else {
            return nil
        }
        
        if let convertedValue = value as? T {
            return convertedValue
        }
        
        throw JSONMapError.badValueType(key: key, value: value)
    }
    
    public func value<T>(withPossibleKeys possibleKeys: [Key]) throws -> T {
        guard possibleKeys.count > 0 else {
            throw JSONMapError.unsuportedFormat
        }
        
        for index in 0...possibleKeys.count - 1 {
            let key = possibleKeys[index]
            do {
                return try self.value(forKey: key)
            } catch {
                if index == possibleKeys.count - 1 {
                    throw error
                }
            }
        }
        
        throw JSONMapError.unsuportedFormat
    }
    
    public func optionalValue<T>(withPossibleKeys possibleKeys: [Key]) throws -> T? {
        guard possibleKeys.count > 0 else {
            throw JSONMapError.unsuportedFormat
        }
        
        for index in 0...possibleKeys.count - 1 {
            let key = possibleKeys[index]
            do {
                let value: T? = try self.optionalValue(forKey: key)
                if value != nil {
                    return value
                }
                
                if index == possibleKeys.count - 1 {
                    return value
                }
            } catch {
                if index == possibleKeys.count - 1 {
                    throw error
                }
            }
        }
        
        throw JSONMapError.unsuportedFormat
    }
    
}
