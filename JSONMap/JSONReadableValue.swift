
import Foundation

public protocol JSONReadableValue {
    
    static func create(with jsonValue: Any) throws -> Self
    
}

public extension JSONReadableValue {
    
    public static func create(with jsonValue: Any) throws -> Self {
        guard let result = jsonValue as? Self else {
            throw JSONMapError.badValueType(key: nil, value: jsonValue)
        }
        
        return result
    }
    
    static func createArray(with jsonValue: Any) throws -> [Self] {
        guard let array = jsonValue as? [Any] else {
            throw JSONMapError.unsuportedFormat
        }
        
        return try array.map({ try self.create(with: $0) })
    }
    
    static func createArrayWithContinueOnError(with jsonArray: [Any]) -> [Self] {
        let results: [Self?] = jsonArray.map({ object in
            do {
                return try self.create(with: object)
            } catch {
                return nil
            }
        })
        
        return results.filter({ $0 != nil }) as! [Self]
    }
    
}

extension String: JSONReadableValue {}

extension Int: JSONReadableValue {}

extension Float: JSONReadableValue {}

extension Double: JSONReadableValue {}
