
import Foundation

public enum JSONMapError: Error {
    
    case unsuportedFormat
    case badValueType(key: Any?, value: Any)
    case noValue(key: Any)
    case badValue(key: Any, value: Any)
    
}
